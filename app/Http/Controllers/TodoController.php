<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class TodoController extends Controller
{
    public function index()
    {
        return view('todo.index');
    }

    public function create()
    {
        return view('todo.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'deadline' => 'required',
        ]);

        $timezone_name = timezone_name_from_abbr("", $request->timezone*60, false);
        $timezone_name = empty($timezone_name) ? 'Asia/Karachi' : $timezone_name;
        date_default_timezone_set($timezone_name);

        $todo = new Todo();
        $todo->description = $request->description;
        $todo->deadline_utc = $this->convertLocalToUTC($request->deadline);
        $todo->deadline_local = date("Y-m-d H:i:s",strtotime($request->deadline));
        $todo->local_timezone = $timezone_name;                
        $todo->save();

        return redirect()->back()->with('success', 'Todo has been added successfully'); 
    }

    public function delete($id)
    {
        $todo = Todo::find($id);
        if (empty($todo)) {
            return redirect()->back()->with('error', 'Todo not found'); 
        } else {
            $todo->delete();
            return redirect()->back()->with('success', 'Todo has been deleted successfully'); 
        }
    }

    public function getTodoData(Request $request)
    {
        $timezone_name = timezone_name_from_abbr("", $request->timezone*60, false);
        $timezone_name = empty($timezone_name) ? 'Asia/Karachi' : $timezone_name;
        $todos = [];
        $todoObj = Todo::get();

        foreach ($todoObj as $obj) {
            $todos[] = ['id' => $obj->id, 'description' => $obj->description, 'deadline' => $this->convertUTCToLocal($obj->deadline_utc,$timezone_name)];
        }
        return response()->json(['data' => $todos]);
    }

    public function convertLocalToUTC($datetime)
    {
        $dateTime = date("Y-m-d H:i:s",strtotime($datetime)); 
        $newDateTime = new \DateTime($dateTime); 
        $newDateTime->setTimezone(new \DateTimeZone("UTC")); 
        $dateTimeUTC = $newDateTime->format("Y-m-d H:i:s");
        return $dateTimeUTC;
    }

    public function convertUTCToLocal($datetime,$timezone)
    {
        $dateTime = date("Y-m-d H:i:s",strtotime($datetime)); 
        $newDateTime = new \DateTime($dateTime); 
        $newDateTime->setTimezone(new \DateTimeZone($timezone)); 
        $dateTimeLocal = $newDateTime->format("h:i a, jS F");        
        return $dateTimeLocal;
    }
}
