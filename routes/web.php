<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TodoController::class, 'index'])->name('home');
Route::get('/add_new_todo', [TodoController::class, 'create'])->name('add-new-todo');
Route::get('/get_todo_data', [TodoController::class, 'getTodoData'])->name('get-todo-data');
Route::get('/delete_todo/{id}', [TodoController::class, 'delete']);
Route::post('/save_new_todo', [TodoController::class, 'store'])->name('save-new-todo');



