<!DOCTYPE html>
<html lang="en">
<head>
    <title>Todo App</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
    <body>

    <div class="jumbotron text-center">
        <h1>To-Do App</h1>
    </div>
    
    <div class="container">
        <div class="row">
            @yield('content')
        </div>
    </div>
    </body>
    @stack('scripts')
</html>
