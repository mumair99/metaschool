@extends('layouts.master')
  
@section('content')
    <div class="row pull-right">
        <a href="{{route('add-new-todo')}}" class="btn btn-success">Add New</a>
    </div>
    <div class="clearfix"></div>
    @if (\Session::has('success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('success') !!}</li>
            </ul>
        </div>
    @endif
    @if (\Session::has('error'))
        <div class="alert alert-danger">
            <ul>
                <li>{!! \Session::get('error') !!}</li>
            </ul>
        </div>
    @endif
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Description</th>
            <th>Deadline</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>


        </tbody>
    </table>
@endsection

@push('scripts')
    <script>
    $(document).ready(function(){
        var timezone_offset_minutes = new Date().getTimezoneOffset();
        timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
        
        $.ajax({
            url: '{{route("get-todo-data")}}',
            data: {timezone: timezone_offset_minutes},
            success: function(response){
                console.log(response);
                $.each(response.data, function(key, value) {
                    var html = '<tr>'+
                        '<td>' + (key + 1) + '</td>' +
                        '<td>' + value.description + '</td>' +
                        '<td>' + value.deadline + '</td>' +
                        '<td><a href="/delete_todo/'+ value.id +'" class="btn btn-sm btn-danger">Delete</a></td>' +
                    '</tr>';   

                    $('tbody').append(html);
                });
            }
        });

    });
    </script>
@endpush