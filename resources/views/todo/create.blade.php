@extends('layouts.master')
  
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li> {{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="{{route('save-new-todo')}}" method="POST">
                @csrf
                <input type="hidden" id="timezone" name="timezone">
                <div class="form-group">
                    <label >Description:</label>
                    <input type="text" class="form-control" name="description" placeholder="Enter Todo Description">
                </div>

                <div class="form-group">
                    <label >Deadline:</label>
                    <input type="datetime-local" class="form-control" name="deadline" placeholder="Enter Todo Deadline">
                </div>

                <button type="submit" class="btn btn-success">Save</button>
                <a href="{{route('home')}}" class="btn btn-primary">Cancel</a>
                
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
    $(document).ready(function(){
        var timezone_offset_minutes = new Date().getTimezoneOffset();
        timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
        $('input#timezone').val(timezone_offset_minutes); 
    });
    </script>
@endpush